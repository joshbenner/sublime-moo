import sublime
import sublime_plugin
import telnetlib
import re


class send_to_moo(sublime_plugin.TextCommand):
    code_session_start = "** Code session initiated **"

    def fail(self, msg):
        sublime.error_message(msg)

    def get_moo_config(self, mooname):
        configs = self.view.settings().get('send_to_moo')
        if (mooname in configs):
            config = configs[mooname]
            require = ['host', 'port', 'user', 'pass']
            for req in require:
                if (req not in config
                    or (type(config[req]) is str and len(config[req]) < 1)):
                    return '{0} missing required conf {1}'.format(mooname, req)
                elif (req == 'port'):
                    try:
                        port = int(config[req])
                    except ValueError:
                        return 'Port must be an integer'
                    config['port'] = port
            return config
        else:
            return "Config for '{0}' not found".format(mooname)

    def end_telnet(self, telnet):
        telnet.write('@quit\n')
        telnet.read_all()

    def get_mooname(self):
        first_line = self.view.substr(self.view.line(sublime.Region(0, 0)))
        match = re.search(r"^/\*\W*moo:\W*([^ ]+)", first_line)
        if (match):
            return match.group(1)
        else:
            return ''

    def run(self, edit):
        mooname = self.get_mooname()
        if (not mooname):
            self.fail("MOO name not specified")
            return
        config = self.get_moo_config(mooname)
        if (type(config) is dict):
            lines = []
            for region in self.view.sel():
                lines.extend(self.view.lines(region))
            if (lines):
                telnet = telnetlib.Telnet(config['host'], config['port'], 5)
                try:
                    telnet.read_very_lazy()
                    login = ('connect {0} {1}\n'
                        .format(config['user'], config['pass']))
                    telnet.write(login)
                    conn_pats = [
                    r"\*\*\* Redirecting old connection to this port \*\*\*"]
                    conn_pats.append(r"\*\*\* Connected \*\*\*")
                    match = telnet.expect(conn_pats, 2)
                    if (match[0] > 0):
                        telnet.write("@start-code-session\n")
                        sess = telnet.read_until(self.code_session_start, 2)
                        if (sess.endswith(self.code_session_start)):
                            for line in lines:
                                telnet.write(self.view.substr(line)
                                    .encode('ascii', 'ignore') + '\n')
                            telnet.write("@end-code-session\n")
                            match = telnet.expect(
                                [r"(\d+) verbs with errors\."], 2)
                            if (match[0] > -1):
                                sublime.status_message("MOO: {0} errors"
                                    .format(match[1].group(1)))
                                print match[2]
                            else:
                                sublime.status_message("ERROR")
                            self.end_telnet(telnet)
                        else:
                            self.end_telnet(telnet)
                            self.fail("Could not start code session")
                    else:
                        self.fail("Could not login")
                except EOFError:
                    self.fail("Socket error")
        else:
            # On error, a failure message is stashed there instead.
            self.fail(config)
